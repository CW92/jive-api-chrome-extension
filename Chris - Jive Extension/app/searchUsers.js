﻿document.addEventListener('DOMContentLoaded', function () {
    var link = document.getElementById('searchDetails');
    // onClick's logic below:
    link.addEventListener('click', function () {
        var xmlhttp = new XMLHttpRequest();
        var xmlhttp = new XMLHttpRequest();
        var searchTerm = document.getElementById('inputSearchTerm').value;
        var endpoint = "/api/core/v3/search/people?filter=search(" + searchTerm + ")";
        var fullUrl = baseUrl + endpoint;

        if (fullUrl != "") {
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var text = xmlhttp.responseText;
                    var myArr = JSON.parse(text.substring(text.indexOf(';') + 1));
                    myFunction(myArr);
                }
            };

            xmlhttp.open("GET", fullUrl, true);
            xmlhttp.setRequestHeader("Authorization", userdetails);
            xmlhttp.send();

            function myFunction(arr) {
                var out = "";
                var i;
                for (i = 0; i < arr.list.length; i++) {
                    out += 'Username: ' + arr.list[i].jive.username + " & User Id: " + arr.list[i].id + '<br/>';
                }
                document.getElementById("searchResultsPeople").innerHTML = out;
            }
        };
    });
});