﻿document.addEventListener('DOMContentLoaded', function () {
    var link = document.getElementById('getBrowseId');
    // onClick's logic below:
    link.addEventListener('click', function () {
        document.getElementById("browseIdLookup").style.display = "none";
            //console.log(username + " : " + password)
            var contentTypeText = document.getElementById('contentType').value;
        
            if (contentTypeText === "Document")
            {
                var contentType = 102;
            } 
            else if (contentTypeText === "Discussion")
            {
                var contentType = 1;
            }
            else if (contentTypeText === "Blog")
            {
                var contentType = 38;
            }
            else
            {
                var contentType = null;
            }

            var contentId = document.getElementById('contentId').value;

            var xmlhttp = new XMLHttpRequest();
            var xmlhttp = new XMLHttpRequest();
            var endpoint = "/api/core/v3/contents?filter=entityDescriptor("+ contentType + "," + contentId + ")";
            var fullUrl = baseUrl + endpoint;

            if (fullUrl != "") {
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        var text = xmlhttp.responseText;
                        var myArr = JSON.parse(text.substring(text.indexOf(';') + 1));
                        myFunction(myArr);
                    }
                };

                xmlhttp.open("GET", fullUrl, true);
                xmlhttp.setRequestHeader("Authorization", userdetails);
                xmlhttp.send();

                function myFunction(arr) {
                    var out = "";
                    var i;
                    for (i = 0; i < arr.list.length; i++) {
                        out += 'Browse Id: ' + arr.list[i].contentID + " & Content Id: " + arr.list[i].id + '<br/>';
                    }
                    document.getElementById("outputBrowseId").innerHTML = out;
                }
            } else {
                console.log("URL is missing or incorrect, URL is currently: " + fullUrl)
            };
        });
});