﻿document.addEventListener('DOMContentLoaded', function () {
    var link = document.getElementById('updateBrowsePlaceUrl');
    // onClick's logic below:
    link.addEventListener('click', function () {
        var xmlhttp = new XMLHttpRequest();
        var xmlhttp = new XMLHttpRequest();
       
        var groupType = document.getElementById('memberType').value;
        var newUrl = document.getElementById('displayName').value;

        var groupId = document.getElementById('groupId').value;
        var endpoint = "/api/core/v3/places/" + groupId;
        var fullUrl = baseUrl + endpoint;

        if (fullUrl != "") {
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var text = xmlhttp.responseText;
                }
            };

            var mimeType = "application/JSON";
            xmlhttp.open("PUT", fullUrl, true);
            xmlhttp.setRequestHeader("Authorization", userdetails);
            xmlhttp.setRequestHeader("Content-Type", mimeType);
            xmlhttp.send("{'type': 'group', 'displayName' : '" + newUrl + "', 'groupType' : '" + groupType + "'}");
        };

        xmlhttp.open("GET", fullUrl, true);
        xmlhttp.setRequestHeader("Authorization", userdetails);
        xmlhttp.send();

        function myFunction(arr) {
            var out = "";
            var i;
            for (i = 0; i < arr.list.length; i++) {
                out += 'URL: ' + arr.list[i].displayName + " <br /> Group Name: " + arr.list[i].name + " <br /> Browse ID: " + arr.list[i].placeID + "<br/><br />";
            }
            document.getElementById("updatedUrl").innerHTML = out;
        };
    });
});