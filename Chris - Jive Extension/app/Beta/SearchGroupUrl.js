﻿document.addEventListener('DOMContentLoaded', function () {
    var link = document.getElementById('searchPlaceDetails');
    // onClick's logic below:
    link.addEventListener('click', function () {
        var xmlhttp = new XMLHttpRequest();
        var xmlhttp = new XMLHttpRequest();
        var searchTerm = document.getElementById('inputPlaceUrlSearchTerm').value;
        var endpoint = "/api/core/v3/search/places?filter=search(" + searchTerm + ")";
        var fullUrl = baseUrl + endpoint;

        if (fullUrl != "") {
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var text = xmlhttp.responseText;
                    var myArr = JSON.parse(text.substring(text.indexOf(';') + 1));
                    myFunction(myArr);
                }
            };

            xmlhttp.open("GET", fullUrl, true);
            xmlhttp.setRequestHeader("Authorization", userdetails);
            xmlhttp.send();

            function myFunction(arr) {
                var out = "";
                var i;
                for (i = 0; i < arr.list.length; i++) {
                    out += 'URL: ' + arr.list[i].displayName + " <br /> Group Name: " + arr.list[i].name + " <br /> Browse ID: " + arr.list[i].placeID + "<br/><br />";
                }
                document.getElementById("searchResultsGroup").innerHTML = out;
            }
        };
    });
});